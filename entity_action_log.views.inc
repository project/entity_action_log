<?php

/**
 * @file
 * Provide views data and handlers for entity_action_log.module.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data().
 */
function entity_action_log_views_data() {
  $data['entity_action_log']['table']['group'] = t('Entity action log');

  // Advertise this table as a possible base table.
  $data['entity_action_log']['table']['base'] = array(
    'field'    => 'eal_lid',
    'title'    => t('Entity action log records'),
    'weight'   => -10,
    'defaults' => array(
      'field' => 'eal_lid',
    ),
  );

  // Lid.
  $data['entity_action_log']['eal_lid'] = array(
    'title'    => t('Record identifier'),
    'help'     => t('Unique record identifier.'),
    'field'    => array(
      'group'          => t('Entity action log'),
      'handler'        => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Nid.
  $data['entity_action_log']['eal_etid'] = array(
    'title'  => t('Node ID'),
    'help'   => t('Node ID.'),
    'field'  => array(
      'handler'        => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort'   => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Uid field.
  $data['entity_action_log']['eal_uid'] = array(
    'title'        => t('User UID'),
    'help'         => t('User UID for the record.'),
    'field'  => array(
      'handler'        => 'views_handler_field_numeric',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_user_name',
    ),
    'sort'   => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_user_uid',
    ),
  );

  // Action.
  $data['entity_action_log']['eal_action'] = array(
    'title'    => t('Action'),
    'help'     => t('The action record entry occurred is associated with.'),
    'field'    => array(
      'group'          => t('Entity action log'),
      'handler'        => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_entity_action_log_action',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Path.
  $data['entity_action_log']['eal_path'] = array(
    'title'    => t('Path'),
    'help'     => t('The path record entry occurred at.'),
    'field'    => array(
      'group'          => t('Entity action log'),
      'handler'        => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Name.
  $data['entity_action_log']['eal_name'] = array(
    'title'    => t('Username'),
    'help'     => t('Username for the record entry.'),
    'field'    => array(
      'group'          => t('Entity action log'),
      'handler'        => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
    'filter'   => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // Created field.
  $data['entity_action_log']['eal_created'] = array(
    'title'  => t('Update timestamp'),
    'help'   => t('The timestamp that the record entry occurred.'),
    'field'  => array(
      'handler'        => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort'   => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_data_alter().
 *
 * @param $data
 */
function entity_action_log_views_data_alter(&$data) {
  $relationships = array();
  $handlers = entity_action_log_handlers();

  // Loop handlers in case we add more in future.
  foreach ($handlers as $type => $handler) {
    if (!empty($handler['relationships'])) {
      foreach ($handler['relationships'] as $info) {
        if (!array_key_exists($info['table'], $relationships)) {
          $relationships[$info['table']] = array();
        }
        if (!array_key_exists($info['field'], $relationships[$info['table']])) {
          $relationships[$info['table']][$info['field']] = array(
            'numeric' => !empty($info['numeric']),
            'types'   => array(),
          );
        }
        $relationships[$info['table']][$info['field']]['types'][$type] = array(
          'operations' => $info['operations'],
        );
      }
    }
  }

  // Add handlers for every referable object.
  foreach ($relationships as $table => $fields) {
    foreach ($fields as $field => $field_info) {
      $field_key = 'eal_etid_' . $table . '_' . $field;

      // Forward reference.
      $types_str = '';
      foreach ($field_info['types'] as $type => $info) {
        if ($types_str != '') {
          $types_str .= ', ';
        }
        $types_str .= $type;
        if ($info['operations'] !== NULL) {
          $types_str .= ' ' . t('(operations: @operations)', array('@operations' => implode(',', $info['operations'])));
        }
      }

      $title = t('Entity action log record reference to @table.@field', array(
        '@table' => $table,
        '@field' => $field,
      ));

      $data['entity_action_log'][$field_key] = array(
        'title'        => $title,
        'relationship' => array(
          'title'              => $title,
          'help'               => '',
          'handler'            => 'views_handler_relationship',
          'base'               => $table,
          'base field'         => $field,
          'label'              => $title,
          'relationship field' => 'eal_etid',
        ),
      );

      $extra_str = '';
      foreach ($field_info['types'] as $type => $info) {
        if ($extra_str != '') {
          $extra_str .= ' OR ';
        }
        $extra_str .= '((entity_action_log.eal_type = \'' . $type . '\')';
        if ($info['operations'] !== NULL) {
          $extra_str .= ' AND (entity_action_log.eal_operation IN (\'' . implode('\',\'', $info['operations']) . '\'))';
        }
        $extra_str .= ')';
      }

      $data['entity_action_log'][$field_key]['relationship']['extra'] = $extra_str;

      $help = t('Relation to Entity action log table');

      // Backward reference.
      $data[$table]['entity_action_log'] = array(
        'title'        => t('Reverse: @title', array('@title' => $title)),
        'help'         => $help,
        'group'        => t('Entity action log'),
        'relationship' => array(
          'title'              => t('Reverse: @title', array('@title' => $title)),
          'help'               => $help,
          'handler'            => 'views_handler_relationship',
          'base'               => 'entity_action_log',
          'base field'         => 'eal_etid',
          'label'              => t('Reverse: @title', array('@title' => $title)),
          'relationship field' => $field,
          'extra'              => str_replace("entity_action_log", "%alias", $extra_str),
        ),
      );
    }
  }
}
