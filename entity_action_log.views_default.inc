<?php
/**
 * @file
 * View for listing variables.
 */

/**
 * Implements hook_views_default_views().
 */
function entity_action_log_views_default_views() {
  $view = new view();
  $view->name = 'entity_action_log';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'entity_action_log';
  $view->human_name = 'Entity action log';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Entity action log';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'access content overview';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'eal_lid' => 'eal_lid',
    'nid' => 'nid',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'eal_lid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'No entity action log records were found matching your criteria.';
  /* Relationship: Entity action log: Entity action log record reference to node.nid */
  $handler->display->display_options['relationships']['eal_etid_node_nid']['id'] = 'eal_etid_node_nid';
  $handler->display->display_options['relationships']['eal_etid_node_nid']['table'] = 'entity_action_log';
  $handler->display->display_options['relationships']['eal_etid_node_nid']['field'] = 'eal_etid_node_nid';
  /* Field: Entity action log: User UID */
  $handler->display->display_options['fields']['eal_uid']['id'] = 'eal_uid';
  $handler->display->display_options['fields']['eal_uid']['table'] = 'entity_action_log';
  $handler->display->display_options['fields']['eal_uid']['field'] = 'eal_uid';
  $handler->display->display_options['fields']['eal_uid']['label'] = 'User';
  $handler->display->display_options['fields']['eal_uid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['eal_uid']['separator'] = '';
  /* Field: Entity action log: Node ID */
  $handler->display->display_options['fields']['eal_etid']['id'] = 'eal_etid';
  $handler->display->display_options['fields']['eal_etid']['table'] = 'entity_action_log';
  $handler->display->display_options['fields']['eal_etid']['field'] = 'eal_etid';
  $handler->display->display_options['fields']['eal_etid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['eal_etid']['separator'] = '';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'eal_etid_node_nid';
  $handler->display->display_options['fields']['title']['label'] = 'Title';
  $handler->display->display_options['fields']['title']['empty'] = 'Nid [eal_etid]';
  /* Field: Entity action log: Action */
  $handler->display->display_options['fields']['eal_action']['id'] = 'eal_action';
  $handler->display->display_options['fields']['eal_action']['table'] = 'entity_action_log';
  $handler->display->display_options['fields']['eal_action']['field'] = 'eal_action';
  $handler->display->display_options['fields']['eal_action']['label'] = 'Action';
  /* Field: Entity action log: Update timestamp */
  $handler->display->display_options['fields']['eal_created']['id'] = 'eal_created';
  $handler->display->display_options['fields']['eal_created']['table'] = 'entity_action_log';
  $handler->display->display_options['fields']['eal_created']['field'] = 'eal_created';
  $handler->display->display_options['fields']['eal_created']['date_format'] = 'short';
  $handler->display->display_options['fields']['eal_created']['second_date_format'] = 'kada_date_only';
  /* Field: Entity action log: Username */
  $handler->display->display_options['fields']['eal_name']['id'] = 'eal_name';
  $handler->display->display_options['fields']['eal_name']['table'] = 'entity_action_log';
  $handler->display->display_options['fields']['eal_name']['field'] = 'eal_name';
  $handler->display->display_options['fields']['eal_name']['label'] = 'Username';
  $handler->display->display_options['fields']['eal_name']['alter']['text'] = '[eal_name]';
  $handler->display->display_options['fields']['eal_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['eal_name']['alter']['path'] = 'user/[eal_uid]';
  /* Sort criterion: Entity action log: Update timestamp */
  $handler->display->display_options['sorts']['eal_created']['id'] = 'eal_created';
  $handler->display->display_options['sorts']['eal_created']['table'] = 'entity_action_log';
  $handler->display->display_options['sorts']['eal_created']['field'] = 'eal_created';
  $handler->display->display_options['sorts']['eal_created']['order'] = 'DESC';
  /* Filter criterion: Entity action log: Node ID */
  $handler->display->display_options['filters']['eal_etid']['id'] = 'eal_etid';
  $handler->display->display_options['filters']['eal_etid']['table'] = 'entity_action_log';
  $handler->display->display_options['filters']['eal_etid']['field'] = 'eal_etid';
  $handler->display->display_options['filters']['eal_etid']['group'] = 1;
  $handler->display->display_options['filters']['eal_etid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['eal_etid']['expose']['operator_id'] = 'eal_etid_op';
  $handler->display->display_options['filters']['eal_etid']['expose']['label'] = 'Node ID';
  $handler->display->display_options['filters']['eal_etid']['expose']['operator'] = 'eal_etid_op';
  $handler->display->display_options['filters']['eal_etid']['expose']['identifier'] = 'eal_etid';
  $handler->display->display_options['filters']['eal_etid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    8 => 0,
    3 => 0,
    7 => 0,
    5 => 0,
    6 => 0,
  );
  /* Filter criterion: Entity action log: User UID */
  $handler->display->display_options['filters']['eal_uid']['id'] = 'eal_uid';
  $handler->display->display_options['filters']['eal_uid']['table'] = 'entity_action_log';
  $handler->display->display_options['filters']['eal_uid']['field'] = 'eal_uid';
  $handler->display->display_options['filters']['eal_uid']['value'] = '';
  $handler->display->display_options['filters']['eal_uid']['group'] = 1;
  $handler->display->display_options['filters']['eal_uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['eal_uid']['expose']['operator_id'] = 'eal_uid_op';
  $handler->display->display_options['filters']['eal_uid']['expose']['label'] = 'User UID';
  $handler->display->display_options['filters']['eal_uid']['expose']['operator'] = 'eal_uid_op';
  $handler->display->display_options['filters']['eal_uid']['expose']['identifier'] = 'eal_uid';
  $handler->display->display_options['filters']['eal_uid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    8 => 0,
    3 => 0,
    7 => 0,
    5 => 0,
    6 => 0,
  );
  /* Filter criterion: Entity action log: Action */
  $handler->display->display_options['filters']['eal_action']['id'] = 'eal_action';
  $handler->display->display_options['filters']['eal_action']['table'] = 'entity_action_log';
  $handler->display->display_options['filters']['eal_action']['field'] = 'eal_action';
  $handler->display->display_options['filters']['eal_action']['exposed'] = TRUE;
  $handler->display->display_options['filters']['eal_action']['expose']['operator_id'] = 'eal_action_op';
  $handler->display->display_options['filters']['eal_action']['expose']['label'] = 'Action';
  $handler->display->display_options['filters']['eal_action']['expose']['operator'] = 'eal_action_op';
  $handler->display->display_options['filters']['eal_action']['expose']['identifier'] = 'eal_action';
  $handler->display->display_options['filters']['eal_action']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['eal_action']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    8 => 0,
    3 => 0,
    7 => 0,
    5 => 0,
    6 => 0,
  );

  /* Display: Node tab */
  $handler = $view->new_display('page', 'Node tab', 'page');
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'No matching entity log records were found for this node.';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Entity action log: User UID */
  $handler->display->display_options['fields']['eal_uid']['id'] = 'eal_uid';
  $handler->display->display_options['fields']['eal_uid']['table'] = 'entity_action_log';
  $handler->display->display_options['fields']['eal_uid']['field'] = 'eal_uid';
  $handler->display->display_options['fields']['eal_uid']['label'] = 'User';
  $handler->display->display_options['fields']['eal_uid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['eal_uid']['separator'] = '';
  /* Field: Entity action log: Action */
  $handler->display->display_options['fields']['eal_action']['id'] = 'eal_action';
  $handler->display->display_options['fields']['eal_action']['table'] = 'entity_action_log';
  $handler->display->display_options['fields']['eal_action']['field'] = 'eal_action';
  $handler->display->display_options['fields']['eal_action']['label'] = 'Action';
  /* Field: Entity action log: Update timestamp */
  $handler->display->display_options['fields']['eal_created']['id'] = 'eal_created';
  $handler->display->display_options['fields']['eal_created']['table'] = 'entity_action_log';
  $handler->display->display_options['fields']['eal_created']['field'] = 'eal_created';
  $handler->display->display_options['fields']['eal_created']['date_format'] = 'short';
  $handler->display->display_options['fields']['eal_created']['second_date_format'] = 'kada_date_only';
  /* Field: Entity action log: Username*/
  $handler->display->display_options['fields']['eal_name']['id'] = 'eal_name';
  $handler->display->display_options['fields']['eal_name']['table'] = 'entity_action_log';
  $handler->display->display_options['fields']['eal_name']['field'] = 'eal_name';
  $handler->display->display_options['fields']['eal_name']['label'] = 'Username';
  $handler->display->display_options['fields']['eal_name']['alter']['text'] = '[eal_name]';
  $handler->display->display_options['fields']['eal_name']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['eal_name']['alter']['path'] = 'user/[eal_uid]';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Entity action log: Node ID */
  $handler->display->display_options['arguments']['eal_etid']['id'] = 'eal_etid';
  $handler->display->display_options['arguments']['eal_etid']['table'] = 'entity_action_log';
  $handler->display->display_options['arguments']['eal_etid']['field'] = 'eal_etid';
  $handler->display->display_options['arguments']['eal_etid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['eal_etid']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['eal_etid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['eal_etid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['eal_etid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['eal_etid']['limit'] = '0';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Entity action log: User UID */
  $handler->display->display_options['filters']['eal_uid']['id'] = 'eal_uid';
  $handler->display->display_options['filters']['eal_uid']['table'] = 'entity_action_log';
  $handler->display->display_options['filters']['eal_uid']['field'] = 'eal_uid';
  $handler->display->display_options['filters']['eal_uid']['value'] = '';
  $handler->display->display_options['filters']['eal_uid']['group'] = 1;
  $handler->display->display_options['filters']['eal_uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['eal_uid']['expose']['operator_id'] = 'eal_uid_op';
  $handler->display->display_options['filters']['eal_uid']['expose']['label'] = 'User UID';
  $handler->display->display_options['filters']['eal_uid']['expose']['operator'] = 'eal_uid_op';
  $handler->display->display_options['filters']['eal_uid']['expose']['identifier'] = 'eal_uid';
  $handler->display->display_options['filters']['eal_uid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    8 => 0,
    3 => 0,
    7 => 0,
    5 => 0,
    6 => 0,
  );
  /* Filter criterion: Entity action log: Action */
  $handler->display->display_options['filters']['eal_action']['id'] = 'eal_action';
  $handler->display->display_options['filters']['eal_action']['table'] = 'entity_action_log';
  $handler->display->display_options['filters']['eal_action']['field'] = 'eal_action';
  $handler->display->display_options['filters']['eal_action']['exposed'] = TRUE;
  $handler->display->display_options['filters']['eal_action']['expose']['operator_id'] = 'eal_action_op';
  $handler->display->display_options['filters']['eal_action']['expose']['label'] = 'Action';
  $handler->display->display_options['filters']['eal_action']['expose']['operator'] = 'eal_action_op';
  $handler->display->display_options['filters']['eal_action']['expose']['identifier'] = 'eal_action';
  $handler->display->display_options['filters']['eal_action']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['eal_action']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    4 => 0,
    8 => 0,
    3 => 0,
    7 => 0,
    5 => 0,
    6 => 0,
  );
  $handler->display->display_options['path'] = 'node/%/entity_action_log';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Entity action log';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $handler->display->display_options['tab_options']['weight'] = '0';

  /* Display: Entity action log */
  $handler = $view->new_display('page', 'Entity action log', 'page_1');
  $handler->display->display_options['path'] = 'admin/content/entity_action_log';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Entity action log';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'management';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  $views[$view->name] = $view;
  return $views;
}
