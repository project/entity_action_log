<?php

/**
 * @file
 * Definition of views_handler_filter_entity_action_log_action.
 */

/**
 * Filter by action.
 *
 * @ingroup views_filter_handlers
 */
class views_handler_filter_entity_action_log_action extends views_handler_filter_in_operator {
  function get_value_options() {
    if (!isset($this->value_options)) {
      $this->value_title = t('Actions');
      $options = entity_action_log_get_actions();
      foreach ($options as $key => $name) {
        $options[$key] = t($name);
      }
      asort($options);
      $this->value_options = $options;
    }
  }
}
